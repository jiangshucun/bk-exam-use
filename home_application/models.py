# -*- coding: utf-8 -*-
"""
Tencent is pleased to support the open source community by making 蓝鲸智云(BlueKing) available.
Copyright (C) 2017 THL A29 Limited, a Tencent company. All rights reserved.
Licensed under the MIT License (the "License"); you may not use this file except in compliance with the License.
You may obtain a copy of the License at http://opensource.org/licenses/MIT
Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.
"""

from django.db import models


class ServerInfo(models.Model):
    bk_host_innerip = models.CharField(max_length=30)
    bk_biz_id = models.CharField(max_length=30)
    cpu_usage = models.FloatField()
    mem_usage = models.FloatField()
    disk_usage = models.FloatField()
    when_created = models.CharField(max_length=100)


class Server(models.Model):
    bk_biz_id = models.CharField(max_length=30)
    bk_host_innerip = models.CharField(max_length=30)
    bk_inst_id = models.CharField(max_length=30)


class liubingServer(models.Model):
    bk_biz_id = models.CharField(max_length=30)
    bk_host_innerip = models.CharField(max_length=30)
    bk_inst_id = models.CharField(max_length=30)
    bk_inst_name = models.CharField(max_length=30)
    bk_host_name = models.CharField(max_length=30)
    bk_os_name = models.CharField(max_length=30)


class JobInfo(models.Model):
    biz_id = models.CharField(max_length=30)
    username = models.CharField(max_length=30)
    job_id = models.CharField(max_length=30)
    createtime = models.CharField(max_length=30)
    bk_host_innerip = models.CharField(max_length=30)
    status = models.CharField(max_length=300)
    log_content = models.CharField(max_length=300)