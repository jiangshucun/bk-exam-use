# -*- coding: utf-8 -*-
"""
Tencent is pleased to support the open source community by making 蓝鲸智云(BlueKing) available.
Copyright (C) 2017 THL A29 Limited, a Tencent company. All rights reserved.
Licensed under the MIT License (the "License"); you may not use this file except in compliance with the License.
You may obtain a copy of the License at http://opensource.org/licenses/MIT
Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.

celery 任务示例

本地启动celery命令: python  manage.py  celery  worker  --settings=settings
周期性任务还需要启动celery调度命令：python  manage.py  celerybeat --settings=settings
"""
import datetime
import re

from celery import task
from celery.schedules import crontab
from celery.task import periodic_task

from blueking.component.shortcuts import get_client_by_user
from home_application.common2 import fast_execute_script, get_task_ip_log, get_job_instance_id
from home_application.models import *
from common.log import logger

# @task()
# def async_task(x, y):
#     """
#     定义一个 celery 异步任务
#     """
#     logger.error(u"celery 定时任务执行成功，执行结果：{:0>2}:{:0>2}".format(x, y))
#     return x + y
#
#
# def execute_task():
#     """
#     执行 celery 异步任务
#
#     调用celery任务方法:
#         task.delay(arg1, arg2, kwarg1='x', kwarg2='y')
#         task.apply_async(args=[arg1, arg2], kwargs={'kwarg1': 'x', 'kwarg2': 'y'})
#         delay(): 简便方法，类似调用普通函数
#         apply_async(): 设置celery的额外执行选项时必须使用该方法，如定时（eta）等
#                       详见 ：http://celery.readthedocs.org/en/latest/userguide/calling.html
#     """
#     now = datetime.datetime.now()
#     logger.error(u"celery 定时任务启动，将在60s后执行，当前时间：{}".format(now))
#     # 调用定时任务
#     async_task.apply_async(args=[now.hour, now.minute], eta=now + datetime.timedelta(seconds=60))
#
#
# @periodic_task(run_every=crontab(minute='*/5', hour='*', day_of_week="*"))
# def get_time():
#     """
#     celery 周期任务示例
#
#     run_every=crontab(minute='*/5', hour='*', day_of_week="*")：每 5 分钟执行一次任务
#     periodic_task：程序运行时自动触发周期任务
#     """
#     execute_task()
#     now = datetime.datetime.now()
#     logger.error(u"celery 周期任务调用成功，当前时间：{}".format(now))
# from home_application.views import format_log_content


# @periodic_task(run_every=crontab(minute='*', hour='*', day_of_week="*"))
def getCapacityData():
    """
    celery 周期任务示例

    run_every=crontab(minute='*/5', hour='*', day_of_week="*")：每 5 分钟执行一次任务
    periodic_task：程序运行时自动触发周期任务
    """
    celery_table = Server.objects.all()
    for celery_ in celery_table:
        check_app = {"app_id": celery_.bk_biz_id,
                     "ip_list": [{"ip": celery_.bk_host_innerip, "bk_cloud_id": celery_.bk_inst_id}]}
        client = get_client_by_user("admin")
        user_name = "admin"
        execute_account = "root"
        script_content = '''#!/bin/bash
        MEMORY=$(free -m | awk 'NR==2{printf "%.2f%%", $3*100/$2 }')
        DISK=$(df -h | awk '$NF=="/"{printf "%s", $5}')
        CPU=$(top -bn1 | grep load | awk '{printf "%.2f%%", $(NF-2)}')
        DATE=$(date "+%Y-%m-%d %H:%M:%S")
        echo -e "$DATE|$MEMORY|$DISK|$CPU"'''
        result = fast_execute_script(check_app, client, user_name, execute_account, script_content, param_content='',
                                     script_timeout=1000)
        if result["result"]:
            script_result = get_task_ip_log(client, check_app["app_id"], result["data"], "admin")
            if script_result:
                keep_log_content(script_result[0]["log_content"], check_app["app_id"], script_result[0]["ip"])


def keep_log_content(log_content, bk_biz_id, bk_host_innerip):
    log_result = log_content.strip("\n").split("|")
    when_created = log_result[0],
    print(when_created[0])
    mem_usage = log_result[1].strip("%")
    disk_usage = log_result[2].strip("%")
    cpu_usage = log_result[3].strip("%")
    ServerInfo.objects.create(bk_biz_id=bk_biz_id, bk_host_innerip=bk_host_innerip, when_created=when_created[0],
                              mem_usage=mem_usage, disk_usage=disk_usage, cpu_usage=cpu_usage)


@task()
def job_zhixin(ip_list, client):
    biz_id = 11
    job_id = 1019
    for i in ip_list:
        result = get_job_instance_id(client, biz_id, i, job_id)
        if result["result"]:
            script_result = get_task_ip_log(client, biz_id, result["data"], "admin")
            if script_result:
                JobInfo_keep_log_content(script_result[0]["log_content"], biz_id, script_result[0]["ip"],
                                         result["data"], script_result[0]["is_success"])


def JobInfo_keep_log_content(log_content, bk_biz_id, bk_host_innerip, job_id, status):
    log_result = log_content.strip("\n")
    date_now_str = str(datetime.datetime.now()).split('.')[0]

    JobInfo.objects.create(bk_host_innerip=bk_host_innerip, createtime=date_now_str,
                           job_id=job_id, status=status, log_content=log_result, username="jiangshucun",
                           biz_id=bk_biz_id)
