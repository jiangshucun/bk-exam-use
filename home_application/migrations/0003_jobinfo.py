# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home_application', '0002_auto_20190107_1237'),
    ]

    operations = [
        migrations.CreateModel(
            name='JobInfo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('biz_id', models.CharField(max_length=30)),
                ('username', models.CharField(max_length=30)),
                ('job_id', models.CharField(max_length=30)),
                ('createtime', models.CharField(max_length=30)),
                ('bk_host_innerip', models.CharField(max_length=30)),
                ('status', models.CharField(max_length=30)),
                ('log_content', models.CharField(max_length=30)),
            ],
        ),
    ]
