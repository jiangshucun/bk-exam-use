# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home_application', '0003_jobinfo'),
    ]

    operations = [
        migrations.AlterField(
            model_name='jobinfo',
            name='log_content',
            field=models.CharField(max_length=300),
        ),
        migrations.AlterField(
            model_name='jobinfo',
            name='status',
            field=models.CharField(max_length=300),
        ),
    ]
