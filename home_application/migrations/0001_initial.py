# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Server',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('bk_biz_id', models.CharField(max_length=30)),
                ('bk_host_innerip', models.CharField(max_length=30)),
                ('bk_inst_id', models.CharField(max_length=30)),
            ],
        ),
        migrations.CreateModel(
            name='ServerInfo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('bk_host_innerip', models.CharField(max_length=30)),
                ('bk_biz_id', models.CharField(max_length=30)),
                ('cpu_usage', models.FloatField()),
                ('mem_usage', models.FloatField()),
                ('disk_usage', models.FloatField()),
                ('when_created', models.CharField(max_length=30)),
            ],
        ),
    ]
