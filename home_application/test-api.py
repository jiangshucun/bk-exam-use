# -*- coding: utf-8 -*-
import settings
import json
import requests
from conf.default import APP_ID, APP_TOKEN, BK_PAAS_HOST

# kwargs = {
#     "bk_app_secret": APP_TOKEN,
#     "bk_app_code": APP_ID,
#     "bk_username": "admin",
#     "ip": {
#         "flag": "bk_host_innerip|bk_host_outerip",
#         "exact": 1,
#         "data": ["192.168.100.186"]  # ip_list
#     },
#     "condition": [
#         {
#             "bk_obj_id": "biz",
#             "fields": [
#                 "default",
#                 "bk_biz_id",
#                 "bk_biz_name",
#             ],
#             # 根据业务ID查询主机
#             "condition": [
#                 # {
#                 #     "field": "bk_biz_id",
#                 #     "operator": "$eq",
#                 #     "value": 1
#                 # }
#             ]
#         }
#     ]
# }
kwargs = {
    "bk_app_code": APP_ID,
    "bk_app_secret": APP_TOKEN,
    "bk_username": "admin",
    "page": {"start": 0, "limit": 10, "sort": "bk_host_id"},
    "ip": {
        "flag": "bk_host_innerip|bk_host_outerip",
        "exact": 1,
        "data": []
    },
    "condition": [
        {
            "bk_obj_id": "host",
            "fields": [
            ],
            "condition": []
        },
        {"bk_obj_id": "module", "fields": [], "condition": []},
        {"bk_obj_id": "set", "fields": [], "condition": [
            {
                "field": "bk_set_id",
                "operator": "$eq",
                "value": 70
            }
        ]},
        {
            "bk_obj_id": "biz",
            "fields": [
                "default",
                "bk_biz_id",
                "bk_biz_name",
            ],
            "condition": [
                {
                    "field": "bk_biz_id",
                    "operator": "$eq",
                    "value": int(11)
                }
            ]
        }
    ]
}

result = requests.post(BK_PAAS_HOST + "/api/c/compapi/v2/cc/search_host/", data=json.dumps(kwargs))
print(result.text)
